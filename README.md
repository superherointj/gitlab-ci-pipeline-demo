# gitlab-ci-demo-pipeline

English:

Whenever `Job A` is triggered, `Job B` should be triggered too.
*Without* having to duplicate the criteria used to trigger Job A (in demo, criteria used was changing file `a.sh`).

The importance of this is, currently I have to replicate upstream trigger rules in downstream jobs, which gets messier the more levels there are. I'm trying to simplify a complex workflow.


Questions:
* Does GitLab-CI have the concept of jobs `triggering` other jobs?

FAQ:
* Should jobs be run together or sequentially?
  - Both. For sequential execution, would add `needs` to Job B.
* Depending on what?
  - Job B depends on whether Job A was triggered.
* Do I need to run b if a is infected?
  - I do not understand what `infected` means, so I don't understand this question.

---

Russian:

Всякий раз, когда запускается задание A, мне нужно также запускать задание B.
Без необходимости дублировать критерии, которые я использую для запуска задания A (в котором критерии изменяют файл a.sh ).
Важность этого заключается в том, что в настоящее время я должен реплицировать правила запуска восходящего потока в последующих заданиях, что становится более запутанным, чем больше уровней.

---

Chinese:

每当触发`作业A'时，我也需要触发`作业B`。
*没有*不必复制我用来触发作业A的标准（该标准正在更改文件`a.sh`）。

这样做的重要性是，目前我必须在下游作业中复制上游触发规则，这使得级别越多越混乱。